package base_scala

import scala.util.parsing.json._

object ReadJson {
    def main(args: Array[String]): Unit = {

        def regJson(json: Option[Any]) = json match {
            // Option 有两个子类别，一个是 Some，一个是 None，当他回传 Some 的时候，
            case Some(map: Map[String, Any]) => map
//            case None => "erro"
//            case other => "Unknow data structure : " + other
        }

        val str = "{\"host\":\"td_test\",\"ts\":1486979192345,\"device\":{\"tid\":\"a123456\",\"os\":\"android\",\"sdk\":\"1.0.3\"},\"time\":1501469230058}"
        val jsonS = JSON.parseFull(str)
        val first = regJson(jsonS)
        // 获取一级key
        println(first.get("host").getOrElse(""))
        // 获取二级key
        val dev = first.get("device")
        println(dev)
        val sec = regJson(dev)
        println(sec.get("tid").toString.replace("Some(", "").replace(")", ""))
    }

}
