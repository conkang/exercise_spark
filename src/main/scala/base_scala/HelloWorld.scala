package base_scala

import scala.collection.immutable.Map

object HelloWorld {

    val fadd = (a: Int, b: Int) => a + b
    val fpower = (a: Int, b: Int) => a * b
    var fabMap: Map[BigInt, BigInt] = Map()

    // 函数作为长的的调用方式
    def tt(f: (Int, Int) => Int, i: Int, j: Int): Int = {
        return f(i, j)
    }


    def main(args: Array[String]): Unit = {
        println(("a", 2))
        println(method1("hello"))
        println(add(1, 2))
        println(f1(1, 3))
        pp("hi")
        this.pp("kzj")
        printString("Runoob", "Scala", "Python")
        println(tt(fadd, 3, 5))
        println(tt(fpower, 3, 5))
        println(fab(15))
    }

    def method1(arg: String): String = {
        return arg
    }

    // 有返回值
    def add(arg1: Int, arg2: Int): Int = {
        return arg1 + arg2
    }

    // 默认参数
    def addInt(a: Int = 5, b: Int = 7): Int = {
        var sum: Int = 0
        sum = a + b

        return sum
    }

    //没有返回值
    def pp(arg: String): Unit = {
        println(arg)
    }

    val f1 = (arg1: Int, arg2: Int) => {
        arg1 + arg2
    }

    // 可变参数
    def printString(args: String*): Unit = {
        for (arg <- args) {
            println(arg)
        }
    }

    // 缓存计算斐波那契数列
    def fab(n: BigInt): BigInt = {
        val temp: BigInt = fabMap.get(n).getOrElse(-1)
        if (temp > -1) {
            return temp
        } else {
            if (n == 1 || n == 2) {
                fabMap += (n -> 1)
                return 1
            } else {
                val temp2 = fab(n - 2) + fab(n - 1)
                fabMap += (n -> temp2)
                return temp2
            }
        }
    }
}