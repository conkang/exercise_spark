package base_scala

import scala.collection.mutable.Set

object SetEx {
    // 默认选用不可变的set
    /**关于Scala中_表示的含义
      * 1. 通配符 import scala.math._
      * 2. 默认值 val a:Int=_ 代表0
      * 3. 集合中任意一个元素 List（1，2，3，4）.filter(_%2==0).map(2*_)
      *
      * */

    def main(args: Array[String]): Unit = {
        val set = Set(1,2,3)
        print(set.add(4))
        println(set.getClass.getName) //
        println(set.min)
    }


}
