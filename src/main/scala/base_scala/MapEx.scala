package base_scala

import scala.collection.mutable.Map

object MapEx{
    def main(args: Array[String]): Unit = {
        val m: Map[String, Int] = Map()
        m += ("a" -> 1)
        m += ("b" -> 1)
        m += ("c" -> 1)
        println(m)
        println(m.keys)
        println(m.values.reduce((i:Int,j:Int)=>i+j))

        val myMap: Map[String, String] = Map("key1" -> "value")
        val value1: Option[String] = myMap.get("key1")
        val value2: Option[String] = myMap.get("key2")
        println(value1.get)
        println(value2.getOrElse("dd"))

    }
}
