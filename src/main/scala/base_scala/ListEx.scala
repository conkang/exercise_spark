package base_scala

object ListEx {

    def main(args: Array[String]): Unit = {
        //List是不可变的immutable
        val a: List[String] = List("a", "b", "c")
        a.foreach((i: String) => print(i + " "))
        println()
        println(a)
        // 构造列表的两个基本单位是 Nil 和 ::
        // Nil是空数组，可以用::将其他元素与Nil进行拼接,形成新的数组
        println(3 :: 2 :: 1 :: Nil)
        // 整型列表
        val nums = 1 :: (2 :: (3 :: (4 :: Nil)))
        println(nums)
        // 空列表
        println(Nil)

        // 你可以使用 ::: 运算符或 List.:::() 方法或 List.concat() 方法来连接两个或多个列表
        println(a ::: (3 :: Nil))
        println(List.concat(a, 3 :: Nil))
        // 填充数据
        println(List.fill(3)("aa"))

        // 表头加元素或列表
        println("2" +: a)
        println(a.::("2"))
        println(a.:::(3 :: Nil))

        var res: (Int, Int) =
            List(2, 5, 8, 1, 2, 6, 9, 4, 3, 5).par.aggregate((0, 0))(
                // 累加-->结果封装到一个类中
                (acc, num) => (acc._1 + num, acc._2 + 1),
                // 将各个累加的结果合并起来
                (combine1, combine2) => (combine1._1 + combine2._1, combine1._2 + combine2._2)

            )

        println(res._1 / res._2.toFloat)
    }
}
