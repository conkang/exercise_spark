package base_scala

class Test(var x: Int, var y: Int) {
    def move(move_x: Int, move_y: Int): Unit = {
        x += move_x
        y += move_y
    }
}

object ClassEx {
    def main(args: Array[String]): Unit = {
        val test: Test = new Test(10, 20)
        test.move(1, 2)
        println(test.x, test.y)

    }
}


