package base_scala

object ArrayEx {

    def main(args: Array[String]): Unit = {
        val a : Array[Int] = Array(1,2,3,5)
        a.foreach((i:Int)=>println("i" + i.toString))
        // 计算最大值
        println(a.reduce((i:Int,j:Int)=>{if (i>j) i else j}))
        // 合并数组
       Array.concat(a,a).map((i:Int)=>i.toString).foreach((i:String)=>print(i + " "))
        Array.range(1,10,2)
        Array.iterate(0,10)((i:Int)=>i+2)

    }

}
