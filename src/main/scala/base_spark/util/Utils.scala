package base_spark.util

import java.io.{File, FileReader}

import com.opencsv.CSVReader

object Utils {

    //获取资源的路径
    def getResourcePath(pathOrDir: String, isText: Boolean = false): String = {
        var path = getClass.getResource(pathOrDir).getPath
        if (isText == true) {
            return path
        }
        path = if (path.startsWith("/")) path.substring(1) else path
        path = String.format("file:///%s", path)
        return path
    }

    // 读取本地csv文件
    def readLocalCsvFile(): Unit = {
        val file: File = new File(Utils.getResourcePath("/csv/cat1.csv", true))
        val csvReader: CSVReader = new CSVReader(new FileReader(file))
        println(csvReader.readNext().toList)
        println(csvReader.readNext().toList)
        println(csvReader.readNext().toList)
        println(csvReader.readNext().toList)
        csvReader.close()

    }

    // spark example文件地址
    def sparkExamplePath(): String = {
        var spark_home = System.getenv("SPARK_HOME")
        String.format("%s\\examples\\src\\main\\resources\\", spark_home)


    }


}
