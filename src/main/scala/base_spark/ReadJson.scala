package base_spark

import base_spark.util.Utils.getResourcePath
import com.alibaba.fastjson.JSON
import org.apache.spark.{SparkConf, SparkContext}
import scala.util.parsing.json.{JSON => sJson}

class Readj {
    val conf: SparkConf = new SparkConf()
    conf.setMaster("local[*]")
        .setAppName("读取json文件")
    val sc = new SparkContext(conf)

    def readIt(path: String): Unit = {
        sc.textFile(getResourcePath(path))
            .cache()
            .map(line => JSON.parseObject(line))
            .map(item => item.getOrDefault("投标企业", ""))
            .foreach(println)

        sc.textFile(getResourcePath(path))
            .map(line => sJson.parseFull(line))

            .map(item => item match {
                case Some(value: Map[String, Any]) => value.get("生产企业").getOrElse("")
            })
            .foreach(println)
    }

}


class User() {
    var name: String = "kzj"
    var age: Int = 30

    def this(name: String, age: Int) {
        this()
        this.name = name
        this.age = age

    }
}

object ReadJson {
    def main(args: Array[String]): Unit = {
        var readj = new Readj()
        readj.readIt("/json/1.json")


    }
}
