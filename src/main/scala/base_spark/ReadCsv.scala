package base_spark

import java.io.StringReader
import base_spark.util.Utils
import com.opencsv.CSVReader
import org.apache.spark.{SparkConf, SparkContext}

class Read {
    val conf: SparkConf = new SparkConf()
    conf.setMaster("local[*]")
        .setAppName("读取csv文件")
    val sc = new SparkContext(conf)

    // 读取csv文件,并求第一列和第三列的和
    def readCsvFile(srcPath: String): Unit = {
        val path: String = Utils.getResourcePath(srcPath)
        val res: Int = sc.textFile(path).map(line => {
            val csvReader = new CSVReader(new StringReader(line))
            val temp = csvReader.readNext().toList
            csvReader.close()
            // 注意这儿不能用return 语句
            temp
        }).map(line => line(0).toInt + line(2).toInt)
            .reduce((x, y) => x + y)

        println(res)
    }
}

object ReadCsv {

    def main(args: Array[String]): Unit = {
        val read = new Read
        read.readCsvFile("/csv/cat1.csv")


    }

}
